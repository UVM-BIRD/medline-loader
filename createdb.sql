-- Copyright 2015 The University of Vermont and State Agricultural
-- College.  All rights reserved.
--
-- Written by Matthew B. Storer <matthewbstorer@gmail.com>
--
-- This file is part of MEDLINE Loader.
--
-- MEDLINE Loader is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- MEDLINE Loader is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with MEDLINE Loader.  If not, see <http://www.gnu.org/licenses/>.

create database if not exists medline;

grant all on medline.* to 'medline'@'localhost' identified by 'medline';

create table if not exists medline.basic (
  partitionKey tinyint unsigned not null,
  pmid int not null,
  pubYear int,
  pubMonth varchar(30),
  authors text,
  journalTitle varchar(500),
  journalISSN varchar(10),
  journalVolume varchar(20),
  journalIssue varchar(20),
  journalPage varchar(20),
  title text,
  abstract text,
  isoAbbreviation varchar(255),
  primary key (pmid, partitionKey),
  index pubYear(pubYear),
  index pubMonth(pubMonth),
  index journalTitle(journalTitle),
  index journalISSN(journalISSN)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists medline.mesh (
  partitionKey tinyint unsigned not null,
  pmid int not null,
  descriptor varchar(255),
  descMajorStatus varchar(3),
  qualifier text,
  qualMajorStatus varchar(3),
  index pmid(pmid),
  index descriptor(descriptor)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists medline.pubType (
  partitionKey tinyint unsigned not null,
  pmid int not null,
  pubType varchar(255),
  index pmid(pmid),
  index pubType(pubType)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists medline.author (
  partitionKey tinyint unsigned not null,
  pmid int not null,
  surname varchar(255),
  givenName varchar(25),
  initials varchar(10),
  affiliation text,
  index pmid(pmid),
  index surname(surname),
  index givenName(givenName),
  index initials(initials)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;
