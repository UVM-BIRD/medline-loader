/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MEDLINE Loader.
 *
 * MEDLINE Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MEDLINE Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MEDLINE Loader.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package edu.uvm.ccts.medline;

import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.util.TimeUtil;
import org.apache.commons.cli.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.ConnectException;
import java.util.Date;
import java.util.Properties;

/**
 * Created by mstorer on 5/15/14.
 */
public class Load {
    private static final Log log = LogFactory.getLog(Load.class);

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    public static void main(String[] args) {
        System.out.println("MEDLINE Loader");
        System.out.println("--------------");
        System.out.println("Copyright 2015 The University of Vermont and State Agricultural College.  All rights reserved.\n");

        if (args.length == 0) {
            showHelp();
            System.exit(0);
        }

        MedlineLoader loader = null;
        try {
            CommandLineParser parser = new BasicParser();
            CommandLine line = parser.parse(getCLIOptions(), args);

            loader = new MedlineLoader();

            long start = System.currentTimeMillis();
            log.info("process started at " + new Date());

            if (line.hasOption("prepare")) {
                loader.prepare();
                log.info("preparing files finished at " + new Date() + " (took " +
                        TimeUtil.formatMsToHMS(System.currentTimeMillis() - start) + ").");

            } else if (line.hasOption("load")) {
                String host = line.hasOption("host") ? line.getOptionValue("host") : "localhost";
                String db = line.hasOption("db") ? line.getOptionValue("db") : "medline";
                String user = line.hasOption("user") ? line.getOptionValue("user") : "medline";
                String pass = line.hasOption("pass") ? line.getOptionValue("pass") : "medline";

                String url = "jdbc:mysql://" + host + "/" + db;

                Properties properties = new Properties();
                properties.put("user", user);
                properties.put("password", pass);

                DataSource dataSource = new DataSource("medline", JDBC_DRIVER, url, properties);

                loader.populateDatabase(dataSource);

                log.info("populating database finished at " + new Date() + " (took " +
                        TimeUtil.formatMsToHMS(System.currentTimeMillis() - start) + ").");
            }

        } catch (Exception e) {
            if (e instanceof ConnectException) {
                String server = loader != null ? loader.getFTPHost() : "FTP server";
                log.error("Couldn't establish connection to " + server + "!");
                log.info("The most common cause for this error is not having obtained a MEDLINE/PubMed lease from NLM.  To learn about and obtain such a lease, see: http://www.nlm.nih.gov/databases/journal.html.");

            } else {
                log.error(e.getMessage(), e);
            }
            System.exit(-1);
        }
    }

    private static void showHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(100);
        formatter.printHelp("medline-loader", getCLIOptions(), true);
    }

    private static Options getCLIOptions() {
        Options options = new Options();

        options.addOption(OptionBuilder.hasArg()
                .withLongOpt("host")
                .withArgName("string")
                .withDescription("the database host (default 'localhost')")
                .create('h'));
        options.addOption(OptionBuilder.hasArg()
                .withLongOpt("db")
                .withArgName("string")
                .withDescription("the database name (default 'medline')")
                .create('d'));
        options.addOption(OptionBuilder.hasArg()
                .withLongOpt("user")
                .withArgName("string")
                .withDescription("the database user name (default 'medline')")
                .create('u'));
        options.addOption(OptionBuilder.hasArg()
                .withLongOpt("pass")
                .withArgName("string")
                .withDescription("the database user password (default 'medline')")
                .create('p'));

        OptionGroup group = new OptionGroup();
        group.addOption(OptionBuilder.withLongOpt("prepare")
                .withDescription("only prepare database files for import")
                .create());
        group.addOption(OptionBuilder.withLongOpt("load")
                .withDescription("only load previously-prepared files into the target database")
                .create());
        group.setRequired(true);
        options.addOptionGroup(group);

        return options;
    }
}
