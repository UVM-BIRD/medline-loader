/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MEDLINE Loader.
 *
 * MEDLINE Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MEDLINE Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MEDLINE Loader.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package edu.uvm.ccts.medline.model;

import electric.xml.Element;

/**
 * Created by mstorer on 5/20/14.
 */
public class MeshData {
    private String descriptor;
    private String descMajorStatus;
    private String qualifier;
    private String qualMajorStatus;

    public MeshData(Element e) {
        Element xmlDescriptor = e.getElement("DescriptorName");
        if (xmlDescriptor != null) {
            descriptor = xmlDescriptor.getTrimTextString();
            descMajorStatus = xmlDescriptor.getAttribute("MajorTopicYN");
        }

        Element xmlQualifier = e.getElement("QualifierName");
        if (xmlQualifier != null) {
            qualifier = xmlQualifier.getTrimTextString();
            qualMajorStatus = xmlQualifier.getAttribute("MajorTopicYN");
        }
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public String getDescMajorStatus() {
        return descMajorStatus;
    }

    public void setDescMajorStatus(String descMajorStatus) {
        this.descMajorStatus = descMajorStatus;
    }

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }

    public String getQualMajorStatus() {
        return qualMajorStatus;
    }

    public void setQualMajorStatus(String qualMajorStatus) {
        this.qualMajorStatus = qualMajorStatus;
    }
}
