/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MEDLINE Loader.
 *
 * MEDLINE Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MEDLINE Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MEDLINE Loader.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package edu.uvm.ccts.medline.model;

import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.XPath;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mstorer on 5/20/14.
 */
public class Record {
    private Integer partitionKey;
    private Integer pmid;
    private BasicData basicData;
    private List<MeshData> meshDataList;
    private List<PubType> pubTypeList;
    private List<Author> authorList;

    public Record(Element e) throws NoSuchAlgorithmException {
        pmid = Integer.parseInt(e.getElement("PMID").getTrimTextString());

        buildPartitionKey(pmid);

        basicData = new BasicData(e);

        meshDataList = new ArrayList<MeshData>();
        Elements xmlMeshHeadings = e.getElements(new XPath("MeshHeadingList/MeshHeading"));
        while (xmlMeshHeadings.hasMoreElements()) {
            meshDataList.add(new MeshData(xmlMeshHeadings.next()));
        }

        pubTypeList = new ArrayList<PubType>();
        Elements xmlPubTypes = e.getElements(new XPath("Article/PublicationTypeList/PublicationType"));
        while (xmlPubTypes.hasMoreElements()) {
            pubTypeList.add(new PubType(xmlPubTypes.next()));
        }

        authorList = new ArrayList<Author>();
        Elements xmlAuthors = e.getElements(new XPath("Article/AuthorList/Author"));
        while (xmlAuthors.hasMoreElements()) {
            authorList.add(new Author(xmlAuthors.next()));
        }
    }

    public Integer getPartitionKey() {
        return partitionKey;
    }

    public Integer getPmid() {
        return pmid;
    }

    public BasicData getBasicData() {
        return basicData;
    }

    public List<MeshData> getMeshDataList() {
        return meshDataList;
    }

    public List<PubType> getPubTypeList() {
        return pubTypeList;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    private void buildPartitionKey(Object obj) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(String.valueOf(obj).getBytes());
        byte[] bytes = md.digest();
        ByteBuffer buf = ByteBuffer.wrap(Arrays.copyOfRange(bytes, bytes.length - 4, bytes.length));
        partitionKey = Math.abs(buf.getInt(0)) % 256;
    }
}
