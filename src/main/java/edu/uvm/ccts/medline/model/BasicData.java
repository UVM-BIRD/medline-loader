/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MEDLINE Loader.
 *
 * MEDLINE Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MEDLINE Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MEDLINE Loader.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package edu.uvm.ccts.medline.model;

import edu.uvm.ccts.common.util.XmlUtil;
import edu.uvm.ccts.medline.util.MedlineDateParser;
import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.XPath;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 5/20/14.
 */
public class BasicData {
    private Integer pubYear;
    private String pubMonth;
    private String authors;
    private String journalTitle;
    private String journalISSN;
    private String journalVolume;
    private String journalIssue;
    private String journalPage;
    private String title;
    private String abstractText;
    private String isoAbbreviation;

    public BasicData(Element e) {
        Element xmlArticle = e.getElement("Article");
        if (xmlArticle != null) {
            title = XmlUtil.getText(xmlArticle, "ArticleTitle");

            Element xmlJournal = xmlArticle.getElement("Journal");
            journalISSN = XmlUtil.getText(xmlJournal, "ISSN");
            journalTitle = XmlUtil.getText(xmlJournal, "Title");
            isoAbbreviation = XmlUtil.getText(xmlJournal, "ISOAbbreviation");

            Element xmlJournalIssue = xmlJournal.getElement("JournalIssue");
            if (xmlJournalIssue != null) {
                journalVolume = XmlUtil.getText(xmlJournalIssue, "Volume");
                journalIssue = XmlUtil.getText(xmlJournalIssue, "Issue");

                Element xmlPubDate = xmlJournalIssue.getElement("PubDate");
                String yearStr = XmlUtil.getText(xmlPubDate, "Year");
                pubYear = yearStr != null ? Integer.parseInt(yearStr) : null;
                pubMonth = XmlUtil.getText(xmlPubDate, "Month");

                // fix to glean pubYear and pubMonth from MedlineDate, if not attainable otherwise
                if (pubYear == null && pubMonth == null && xmlPubDate.hasElement("MedlineDate")) {
                    String medlineDate = XmlUtil.getText(xmlPubDate, "MedlineDate");
                    PubDate pubDate = MedlineDateParser.parseMedlineDate(medlineDate);
                    if (pubDate != null) {
                        pubYear = pubDate.getYear();
                        pubMonth = pubDate.getMonth();
                    }
                }
            }

            List<String> authorList = new ArrayList<String>();
            Elements xmlAuthors = xmlArticle.getElements(new XPath("AuthorList/Author"));
            while (xmlAuthors.hasMoreElements()) {
                Element xmlAuthor = xmlAuthors.next();
                String lastName = XmlUtil.getText(xmlAuthor, "LastName");
                String foreName = XmlUtil.getText(xmlAuthor, "ForeName");

                if (foreName != null) {
                    authorList.add(lastName + ", " + foreName);

                } else {
                    authorList.add(lastName);
                }
            }
            authors = StringUtils.join(authorList, "; ");

            journalPage = XmlUtil.getText(xmlArticle, "Pagination/MedlinePgn");

            abstractText = XmlUtil.getText(xmlArticle, "Abstract/AbstractText");
        }

        if (abstractText == null) {
            abstractText = XmlUtil.getText(e, "OtherAbstract/AbstractText");
        }
    }

    public Integer getPubYear() {
        return pubYear;
    }

    public String getPubMonth() {
        return pubMonth;
    }

    public String getAuthors() {
        return authors;
    }

    public String getJournalTitle() {
        return journalTitle;
    }

    public String getJournalISSN() {
        return journalISSN;
    }

    public String getJournalVolume() {
        return journalVolume;
    }

    public String getJournalIssue() {
        return journalIssue;
    }

    public String getJournalPage() {
        return journalPage;
    }

    public String getTitle() {
        return title;
    }

    public String getAbstractText() {
        return abstractText;
    }

    public String getIsoAbbreviation() {
        return isoAbbreviation;
    }
}
