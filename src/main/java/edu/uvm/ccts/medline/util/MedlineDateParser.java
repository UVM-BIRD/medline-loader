/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MEDLINE Loader.
 *
 * MEDLINE Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MEDLINE Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MEDLINE Loader.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package edu.uvm.ccts.medline.util;

import edu.uvm.ccts.medline.model.PubDate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mstorer on 8/12/14.
 */
public class MedlineDateParser {
    private static final Log log = LogFactory.getLog(MedlineDateParser.class);

    private static final Pattern YEAR_MONTH = Pattern.compile("^(\\d{4})\\s+([a-z]+).*$", Pattern.CASE_INSENSITIVE);
    private static final Pattern YEAR_ONLY = Pattern.compile("^(\\d{4}).*$");

    public static PubDate parseMedlineDate(String medlineDate) {
        Matcher m = YEAR_MONTH.matcher(medlineDate);
        if (m.matches()) return new PubDate(Integer.parseInt(m.group(1)), m.group(2));

        m = YEAR_ONLY.matcher(medlineDate);
        if (m.matches()) return new PubDate(Integer.parseInt(m.group(1)), null);

        log.warn("couldn't handle MedlineDate '" + medlineDate + "' - returning null");
        return null;
    }
}
