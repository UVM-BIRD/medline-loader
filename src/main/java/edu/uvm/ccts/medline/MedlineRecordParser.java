/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MEDLINE Loader.
 *
 * MEDLINE Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MEDLINE Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MEDLINE Loader.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package edu.uvm.ccts.medline;

import edu.uvm.ccts.common.db.parser.AbstractXMLFileParser;
import edu.uvm.ccts.common.db.parser.TableData;
import edu.uvm.ccts.medline.model.*;
import electric.xml.Element;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * Created by mstorer on 5/19/14.
 */
public class MedlineRecordParser extends AbstractXMLFileParser {
    public static final String TABLE_FILE_BASIC = "basic.txt";
    public static final String TABLE_FILE_AUTHOR = "author.txt";
    public static final String TABLE_FILE_MESH = "mesh.txt";
    public static final String TABLE_FILE_PUBTYPE = "pubtype.txt";

    private TableData tBasic;
    private TableData tMesh;
    private TableData tPubType;
    private TableData tAuthor;
    private List<TableData> tableDataList;


    public MedlineRecordParser(int threadId, String tempDir, String outputDir) throws IOException {
        super(threadId);

        tBasic =    new TableData(tempDir, outputDir, TABLE_FILE_BASIC);
        tMesh =     new TableData(tempDir, outputDir, TABLE_FILE_MESH);
        tPubType =  new TableData(tempDir, outputDir, TABLE_FILE_PUBTYPE);
        tAuthor =   new TableData(tempDir, outputDir, TABLE_FILE_AUTHOR);

        tableDataList = Arrays.asList(tBasic, tMesh, tPubType, tAuthor);
    }

    @Override
    protected List<TableData> getTableDataList() {
        return tableDataList;
    }

    @Override
    protected InputStream getInputStream(String filename) throws IOException {
        return new GZIPInputStream(new FileInputStream(filename));
    }

    @Override
    protected String getRecordElementName() {
        return "MedlineCitation";
    }

    @Override
    protected void processRecord(Element record) throws Exception {
        updateTables(new Record(record));
    }

    private void updateTables(Record r) throws IOException {
        int pk = r.getPartitionKey();
        int pmid = r.getPmid();

        BasicData b = r.getBasicData();
        tBasic.addRecord(pk, pmid, b.getPubYear(), b.getPubMonth(), b.getAuthors(), b.getJournalTitle(),
                b.getJournalISSN(), b.getJournalVolume(), b.getJournalIssue(), b.getJournalPage(),
                b.getTitle(), b.getAbstractText(), b.getIsoAbbreviation());

        for (MeshData md : r.getMeshDataList()) {
            tMesh.addRecord(pk, pmid, md.getDescriptor(), md.getDescMajorStatus(), md.getQualifier(), md.getQualMajorStatus());
        }

        for (PubType pt : r.getPubTypeList()) {
            tPubType.addRecord(pk, pmid, pt.getPubType());
        }

        for (Author a : r.getAuthorList()) {
            tAuthor.addRecord(pk, pmid, a.getSurname(), a.getGivenName(), a.getInitials(), a.getAffiliation());
        }
    }
}
