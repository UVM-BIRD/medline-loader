/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MEDLINE Loader.
 *
 * MEDLINE Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MEDLINE Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MEDLINE Loader.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package edu.uvm.ccts.medline;

import edu.uvm.ccts.common.db.loader.AbstractFTPLoader;
import edu.uvm.ccts.common.db.parser.AbstractFileParser;

import java.io.IOException;
import java.util.*;

/**
 * Created by mstorer on 5/15/14.
 */
public class MedlineLoader extends AbstractFTPLoader {
    private static final String outputDir = "./out";

    private static final Map<String, String> tableFileMap = new LinkedHashMap<String, String>();
    static {
        tableFileMap.put("basic", MedlineRecordParser.TABLE_FILE_BASIC);
        tableFileMap.put("mesh", MedlineRecordParser.TABLE_FILE_MESH);
        tableFileMap.put("pubType", MedlineRecordParser.TABLE_FILE_PUBTYPE);
        tableFileMap.put("author", MedlineRecordParser.TABLE_FILE_AUTHOR);
    }


    private Properties properties;

    protected MedlineLoader() throws IOException {
        super(outputDir);

        properties = new Properties();
        properties.load(this.getClass().getResourceAsStream("/system.properties"));
    }

    @Override
    protected String getName() {
        return "medline";
    }

    @Override
    protected int getRequiredMemPerThreadMB() {
        return 200;
    }

    @Override
    protected AbstractFileParser buildParser(int threadId, String tempDir) throws IOException {
        return new MedlineRecordParser(threadId, tempDir, outputDir);
    }

    @Override
    protected Map<String, String> getTableFileMap() {
        return tableFileMap;
    }

    @Override
    protected String getFTPHost() {
        return properties.getProperty("nih.ftp.host");
    }

    @Override
    protected String getFTPUser() {
        return properties.getProperty("nih.ftp.user");
    }

    @Override
    protected String getFTPPass() {
        return properties.getProperty("nih.ftp.pass");
    }

    @Override
    protected List<String> getFilenameFilters() {
        return Arrays.asList(
                "/nlmdata/.medleasebaseline/gz/*.xml.gz"
        );
    }
}
